import pygame, sys

pygame.init()
clock = pygame.time.Clock()
root = pygame.display.set_mode((800,600), 0)

while True:
    clock.tick(60)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                print("hi! left!")
            if event.key == pygame.K_RIGHT:
                print("hi! right!")
            if event.key == pygame.K_ESCAPE:
                sys.exit()
    pygame.display.update()
